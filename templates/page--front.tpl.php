<?php include ($directory."/includes/header.tpl.php"); ?>
<?php include ($directory."/includes/slider.tpl.php"); ?>
<div class="container clearfix">
	<?php print $messages; ?>
	<?php if ($page['homepage_header']): ?><!-- / start homepage header block -->
		<div class="full page-header">
			<?php print render($page['homepage_header']); ?>
		</div> <!-- / end homepage header -->
		<div class="clear"></div>
	<?php endif; ?>
	<div class="full">
	<?php if ($page['homepage_one']): ?><!-- / start homepage first block -->
		<div class="home_service one_three text_center">
			<?php print render($page['homepage_one']); ?>
		</div> <!-- / end homepage first block -->
	<?php endif; ?>

	<?php if ($page['homepage_two']): ?><!-- / start homepage second block -->
		<div class="home_service one_three text_center">
			<?php print render($page['homepage_two']); ?>
		</div> <!-- / end homepage first block -->
	<?php endif; ?>


	<?php if ($page['homepage_three']): ?><!-- / start homepage Third block -->
		<div class="home_service one_three text_center">
			<?php print render($page['homepage_three']); ?>
		</div> <!-- / end homepage first block -->
	<?php endif; ?>
	</div>
	<div class="clear"></div>
		<?php if ($page['homepage_content']): ?><!-- / start homepage content block -->
		<div class="full">
			<?php print render($page['homepage_content']); ?>
		</div> <!-- / end homepage content block -->
	<?php endif; ?>
	<div class="clear"></div>
</div> <!-- /.container -->
<?php include ($directory."/includes/footer.tpl.php"); ?>
