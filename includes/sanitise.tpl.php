<?php
function santise_val_url($url) {
  if($url != '') {
    // sanitise (remove all illegal characters) from url
    $url = filter_var($url, FILTER_SANITIZE_URL);

    // validate url
    if (filter_var($url, FILTER_VALIDATE_URL) == true) {
      $url = $url;
    } else {
      $url = '#';
    }
    return $url;
  }

}

$fb_url = santise_val_url(theme_get_setting('facebook_username'));
$twitter_url = santise_val_url(theme_get_setting('twitter_username'));
$instagram_url = santise_val_url(theme_get_setting('instagram_username'));
$youtube_url = santise_val_url(theme_get_setting('youtube_username'));
$googleplus_url = santise_val_url(theme_get_setting('googleplus_username'));
?>
