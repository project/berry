<header id="header">
	<div class="container" class="clearfix">
		<?php if ($logo): ?>
			<div id="logo">
		  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" class="logo">
			<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
		  </a>
		</div> <!-- /#logo -->
		<?php endif; ?>
		<div id="social">
			<?php if (theme_get_setting('social_icons')): ?>
				<?php include ($directory."/includes/sanitise.tpl.php"); ?>
				<?php if($googleplus_url != ''): ?>
					<li><a href="<?php echo $googleplus_url ?>" class="icon" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
				<?php endif; ?>

				<?php if($youtube_url != ''): ?>
					<li><a href="<?php echo $youtube_url ?>" class="icon" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
				<?php endif; ?>

				<?php if($instagram_url != ''): ?>
					<li><a href="<?php echo $instagram_url ?>" class="icon" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
				<?php endif; ?>

				<?php if($twitter_url != ''): ?>
					<li><a href="<?php echo $twitter_url ?>" class="icon" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<?php endif; ?>

				<?php if($fb_url != ''): ?>
					<li><a href="<?php echo $fb_url ?>" class="icon" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<?php endif; ?>
			<?php endif; ?> <!--  end condition to show all icons -->
		<?php if ($page['search_box']): ?><!-- start search box region -->
			<li>
				<div class="search-box">
					<?php print render($page['search_box']); ?>
				</div> <!-- end search box region -->
			</li>
		<?php endif; ?>
		</div> <!-- end #social -->

	</div> <!-- /.container -->
</header>
<nav id="navigation" class="navigation">
	<div class="container" class="clearfix">
		<?php
		$main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
		print drupal_render($main_menu_tree);
		?>
	</div>
</nav> <!-- /#navigation -->
