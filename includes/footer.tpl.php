<footer id="footer" role="contentinfo" class="clearfix">
	<div class="container clearfix">
		<?php if ($page['footer_one']): ?>
			<div class="footer-block one_three">
				<?php print render($page['footer_one']); ?>
			</div>
		<?php endif; ?>

		<?php if ($page['footer_two']): ?>
			<div class="footer-block one_three">
				<?php print render($page['footer_two']); ?>
			</div>
		<?php endif; ?>

		<?php if ($page['footer_three']): ?>
			<div class="footer-block one_three">
				<?php print render($page['footer_three']); ?>
			</div>
		<?php endif; ?>
	</div> <!-- /.container -->
</footer> <!-- /#footer -->
<div class="clear"></div>
<div id="footer-bottom">
	<div class="container" class="clearfix">
		<?php if (theme_get_setting('footer_copyright')): ?>
			<div id="copyright" class="full">Copyright &copy; <?php echo date("Y"); ?>, <?php print $site_name; ?></div>
		<?php endif; ?>
		<?php if ($page['footer']): ?>
			<div class="footer-bottom-block full">
				<?php print render($page['footer']) ?>
			</div> <!--/.footer-bottom-block -->
		<?php endif; ?>
	</div> <!-- /.container -->
</div> <!-- /#footer-bottom -->
