<?php if (theme_get_setting('breadcrumb_link')): ?>
<div id="breadcrumb">
	<div id="container" class="clearfix">
		<?php if ($breadcrumb): print $breadcrumb; endif;?>
	</div>
</div> <!-- /#breadcrumb -->
<?php endif; ?>
