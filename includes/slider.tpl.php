<!-- START SLIDER -->
<?php if (theme_get_setting('home_slider')): ?>
<div class="container clearfix">
<div id="slider">
        <div class="main_view">
            <div class="window">
                <div class="image_reel">
                <img src="<?php echo theme_get_setting('slider_one_image'); ?>" />
                <img src="<?php echo theme_get_setting('slider_two_image'); ?>" />
                <img src="<?php echo theme_get_setting('slider_three_image'); ?>" />
                </div>
				<div class="slider-title">
                    <div class="slidertitle"><?php echo theme_get_setting('slider_one_title'); ?></div>
                    <div class="slidertitle"><?php echo theme_get_setting('slider_two_title'); ?></div>
                    <div class="slidertitle"><?php echo theme_get_setting('slider_three_title'); ?></div>
                </div>
                <div class="slider-text">
                    <div class="slidertext" style="display: none;"><?php echo theme_get_setting('slider_one_desc'); ?></div>
                    <div class="slidertext" style="display: none;"><?php echo theme_get_setting('slider_two_desc'); ?></div>
                    <div class="slidertext" style="display: none;"><?php echo theme_get_setting('slider_three_desc'); ?></div>
                </div>

            </div>

            <div class="paging">
                <a rel="1" href="#">1</a>
                <a rel="2" href="#">2</a>
                <a rel="3" href="#">3</a>
            </div>
        </div>
</div> <!-- #slider -->
</div> <!-- /.container -->
<?php endif; ?>
<div class="clear"></div>
<!-- END SLIDER -->
